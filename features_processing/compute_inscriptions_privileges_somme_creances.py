import pandas as pd, numpy as np
import csv
data = pd.read_csv('data/inscriptions_privileges_max_evt.csv')
data = data[['etat' != 'R']]
data = data.groupby(['siren']).agg({'montant_creance': "sum", 'date_evenement' : "max"})
data = data.rename(columns={'date_evenement' :'date_evenement_max' },inplace=True)
inscriptions_privileges_somme_creance = data[['siren','date_evenement_max','montant_creance']]
