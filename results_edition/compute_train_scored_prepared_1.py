import pandas as pd
import csv

data = pd.read_csv('data/score_normalization.csv')
data['note'] = data.apply(lambda row : round(100 - row['proba_percentile_normalized']),axis=1)

def process(row):
    # In 'cell' mode, the process function must return
    # a single cell value for each row,
    # which will be affected to a new column.
    # The 'row' argument is a dictionary of columns of the row

    if (row['procedure_collective']==True and int(row['note'])>=19):
        note = 19
    else:
        note = row['note']
    return note

data['note'] = data.apply(lambda row : process (row), axis=1)

data = data.drop(['proba_false'],axis=1)

def process(row):
    # In 'cell' mode, the process function must return
    # a single cell value for each row,
    # which will be affected to a new column.
    # The 'row' argument is a dictionary of columns of the row
    # tranche_nom = {k:'score_'+str(k) for k in range(1,8)}
    tranche_fonc = {}
    c = 1
    for i in range(0,101):
        tranche_fonc[i] = c
        if (i%15==0) and (i!=0):
            c+=1
        # res = tranche_fonc[int(row['note'])]
    n = int(row['note'])
    return tranche_fonc[n]

data['tranche'] = data.apply(lambda row : process(row),axis=1)

def process(row):
    # In 'cell' mode, the process function must return
    # a single cell value for each row,
    # which will be affected to a new column.
    # The 'row' argument is a dictionary of columns of the row
    tranche_label = {
    1: "Très faible",
    2: "Faible",
    3: "Assez faible",
    4: "Acceptable",
    5: "Assez forte",
    6: "Forte",
    7: "Très forte"}
    tranche_nom = {1:'risque maximal', 2:'risque tres eleve', 3:'risque fort',
                   4:'risque marque', 5:'risque modere', 6:'risque limite', 7:'risque faible'}
    return tranche_label[int(row['tranche'])]

data['solidite_financiere'] = data.apply(lambda row: process(row),axis=1)

calcul_note = data.drop(['tranche'],axis=1)
