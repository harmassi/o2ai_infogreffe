import pandas as pd
from datetime import datetime, tzinfo
from dateutil.parser import parse
import pytz
utc=pytz.UTC

def process(row):

    correct_date = row['date_premiere_immatriculation'] if pd.notna(row['date_premiere_immatriculation']) else row['date_immatriculation']
    return correct_date
data = pd.read_csv('data/infos_societes.csv')
data = data.head()
data['correct_immatriculation'] = data.apply(lambda row : process(row),axis=1)

index_nan = data[ pd.isna(data['correct_immatriculation']) ].index.values.tolist()
last_it = data[data.index.isin(index_nan)]
data_nan =  data[data.isin(last_it)]
print (data_nan)

data['correct_immatriculation'] = data['correct_immatriculation'].apply(lambda x : parse (str(x),yearfirst = True)
if pd.notna(x) else pd.NaT)
data['correct_immatriculation'] =data['correct_immatriculation'].apply(lambda x :datetime.strftime(x,'%Y-%m-%dT%H:%M:%S'+".000Z")
if pd.notna(x) else pd.NaT)

infos_societes_correct_immatriculations = data.drop(['date_immatriculation', 'date_premiere_immatriculation'], axis=1)
print(infos_societes_correct_immatriculations)