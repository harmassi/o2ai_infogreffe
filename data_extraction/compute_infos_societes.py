import pandas as pd, numpy as np
import json
import requests
from pandas.io.json import json_normalize
import csv
import itertools
from datetime import datetime, timedelta, date
from collections import OrderedDict

columns = ['siren', 'date_immatriculation', 'date_premiere_immatriculation', 'secteur']
schema = [{"name": str(c), "type": "string"} for c in columns]

#replace functions : create instance form classe Dataset and write shema
#infos_societes = dataiku.Dataset("infos_societes")
#infos_societes.write_schema(schema)


def getComptes():
    URL_api = "https://opendata.datainfogreffe.fr/api/records/1.0/download/?dataset=informations-societes" \
              + "&format=json&apikey=65b42899cf863dc85b14b8c7989c09a5a3379607f22b6502bc0123e7;"

    r = requests.get(URL_api)
    data = json.dumps(r.json())
    parsed_data = json.loads(data)
    df = pd.DataFrame(parsed_data)
    return (df)


df = getComptes()
normalized_data = json_normalize(df['fields'])
data_listed = normalized_data['fields'].tolist()
temp_data = pd.DataFrame(data=data_listed)
final_df = pd.DataFrame(data=temp_data, columns=columns)

#export the final_df to the Dataset

#infos_societes.write_from_dataframe(final_df)







