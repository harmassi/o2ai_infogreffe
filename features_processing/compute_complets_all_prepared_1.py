import pandas as pd, numpy as np
import csv

data = pd.read_csv('data/complets_stacked.csv')
indexNames = data[ ~data['millesime'].str.isdigit() ].index
data = data.drop(indexNames , inplace=True)

data.rename(columns={ 'bdl':'capitaux_propres','bee':'total_bilan', 'a2x':'total_actifs', 'a2t':'actif_circulants',
                      'bec':'total_dettes', 'a2n':'val_mob_pla', 'a2p':'cpp_disponibilite', 'bdx':'dettes_fournisseurs',
                      'bds' :'emprunts_obligatoires', 'bdt':'autres_emprunts_obligatoires', 'bdu':'emprunts_etablissement_credits',
                      'bdv':'emprunts_divers', 'bdy':'dettes_fiscales_sociales', 'beh':'dettes_financieres_ct', 'a2c':'total_immobilisation',
                      'cgu':'charges_financieres', 'cfl':'chiffre_affaires_net', 'myp':'effectif_moyen', 'cfr':'total_prod_exp',
                      'c1f':'chiffre_affaires_net_1', 'cga':'amortissements', 'bdi':'resultat'

}, inplace=True)

data['disponibilite'] = round(data[['cpp_disponibilite', 'val_mob_pla']].sum(axis=1))
data['passif_circulants'] = data[['dettes_fournisseurs', 'dettes_fiscales_sociales']].sum(axis=1,min_count=1)
data['type']= 2050

complets_prepared = data[['denomination',
'siren',
'forme_juridique',
'code_ape',
'departement',
'code_greffe',
'date_immatriculation',
'date_de_publication',
'type',
'bigint',
'millesime',
'date_depot',
'date_cloture',
'confidentiel',
'total_immobilisation',
'disponibilite',
'actif_circulants',
'total_actifs',
'resultat',
'capitaux_propres',
'passif_circulants',
'total_dettes',
'dettes_financieres_ct'
'chiffre_affaires_net'
'chiffre_affaires_net_1'
'total_prod_exp'
'amortissements'
'charges_financieres',
'effectif_moyen']]
