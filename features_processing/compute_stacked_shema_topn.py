import pandas as pd, numpy as np
import csv

data = pd.read_csv('data/stacked_shema.csv')

data = data.sort_values(by=['millesime','date_cloture','type','confidentiel','date_de_publication'],ascending=False).groupby('siren').first()

stacked_schema_topn = data