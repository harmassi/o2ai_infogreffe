import pandas as pd, numpy as np
import csv

data = pd.read_csv('data/simplifies_stacked.csv')
data.rename(columns={'142': 'capitaux_propres', '180': 'total_bilan', '113':'total_actifs', '96':'actif_circulants',
                     '83':'val_mob_pla', '87':'cpp_disponibilite', '49':'total_immobilisation',
                     '294':'charges_financieres', '176':'total_dettes', '169':'dettes_financieres_ct',
                     '195':'dettes_plus_an', '376':'effectif_moyen', '210':'ventes_marchandises', '214':'vendu_biens',
                     '218':'vendu_services', '222':'prod_stockee', '224':'prod_immobilisee','230':'autres_prod',
                     '232':'total_prod_exp','226':'subvention', 'n33':'ventes_marchandises_1', 'n34':'vendu_biens_1',
                     'n35':'vendu_services_1', '254':'amortissements','91':'caisse', '172':'autres_dettes', '156':'emprunts',
                     '164':'avances', '166':'dettes_fournisseurs_cpts', '136':'resultat'}, inplace=True)

data = data.fillna({'val_mob_pla' : 0,'caisse' : 0,'cpp_disponibilite' : 0})

data['disponibilite']= data.apply(lambda row : row ['val_mob_pla']+ row ['caisse']+ row['cpp_disponibilite'],axis=1)

def not_null(x):
    return x if pd.notna(x) else 0

#def process(row):
#    passif_circulants = row['dettes_fournisseurs_cpts'] if (pd.isna(row['autres_dette']) & pd.isna(row['autres_dettes_n']) &
#    pd.isna(row['dettes_fournisseurs_cpts'])) else not_null(row ['dettes_fournisseurs_cpts']) + not_null(row ['autres_dettes'])\
#                                                + not_null(row ['autres_dettes_n'])
#    return passif_circulants

#data['passif_circulants'] = data[['dettes_fournisseurs_cpts','autres_dettes','autres_dettes_n']].sum(axis=1,min_count=1)

data['passif_circulants'] = data[['dettes_fournisseurs_cpts','autres_dettes']].sum(axis=1,min_count=1)

data['chiffre_affaires_net'] = data[['ventes_marchandises','vendu_biens','vendu_services']].sum(axis=1,min_count=1)

data['chiffre_affaires_net_1'] = data[['ventes_marchandises_1', 'vendu_biens_1', 'vendu_services_1']].sum(axis=1)

data['type']= 2033

simplifies_prepared = data[['denomination',
'siren',
'forme_juridique',
'code_ape',
'departement',
'code_greffe',
'date_immatriculation',
'date_de_publication',
'type',
'millesime',
'date_depot',
'date_cloture',
'confidentiel',
'total_immobilisation',
'disponibilite',
'actif_circulants',
'total_actifs',
'resultat',
'capitaux_propres',
'dettes_financieres_ct',
'passif_circulants',
'total_dettes',
'chiffre_affaires_net',
'chiffre_affaires_net_1',
'total_prod_exp',
'amortissements',
'charges_financieres',
'effectif_moyen',
]]
