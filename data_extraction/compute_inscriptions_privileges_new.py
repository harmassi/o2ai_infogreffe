import pandas as pd, numpy as np
import json
import requests
from pandas.io.json import json_normalize
import csv
import itertools
from datetime import datetime, timedelta, date
from collections import OrderedDict

columns = ['siren', 'denomination','etat','date_radiation_inscription','date_inscription', 'num_inscription','montant_creance']
schema =  [{"name":str(c), "type":"string"} for c in columns]

#replace DSS functions : create instance "inscriptions_privileges_new" form class Dataset and write shema
#inscriptions_privileges = dataiku.Dataset("inscriptions_privileges_new")
#inscriptions_privileges.write_schema(schema)


def getComptes(month):

    URL_api = "https://opendata.datainfogreffe.fr/api/records/1.0/download/?dataset=inscriptions-privileges"\
            +"&format=json&refine.date_publication="+month\
            +"&apikey=65b42899cf863dc85b14b8c7989c09a5a3379607f22b6502bc0123e7;"
    r = requests.get(URL_api)
    data=json.dumps(r.json())
    parsed_data=json.loads(data)
    df=pd.DataFrame(parsed_data)
    return (df)


# Extract historical dates
today_date = date.today()
current_month = today_date.strftime("%Y-%m")
df = getComptes(current_month)

normalized_data=json_normalize(df['fields'])
data_listed=normalized_data['fields'].tolist()
temp_data = pd.DataFrame(data=data_listed)
final_df=pd.DataFrame(data=temp_data, columns=columns)

# replace function : write data from final dataframe to the instance of Dataset "inscriptions_privileges_new"

#inscriptions_privileges.write_from_dataframe(final_df)
