import pandas as pd, numpy as np
import json
import requests
from pandas.io.json import json_normalize
import csv
import itertools
from datetime import datetime, timedelta, date
from collections import OrderedDict


# Column preparation
columns = ['denomination', 'siren', 'nic', 'forme_juridique', 'code_ape', 'libelle_ape', 'adresse',
           'code_postal', 'ville', 'num_dept', 'departement', 'region', 'code_greffe', 'greffe',
           'date_immatriculation', 'date_radiation', 'statut', 'geolocalisation',
           'date_de_publication', 'type', 'millesime', 'date_depot', 'date_cloture',
           'date_cloture_n_1', 'date_saisie', 'confidentiel', 'type_anomalie', 'devise', 'duree_exercice',
           'duree_exercice_n_1', 'aaa', 'a1h', 'a2z', 'aab', 'aac', 'a1j', 'a3a', 'aad', 'aae', 'a1k', 'a3b',
           'aaf', 'aag', 'a1l', 'a3c', 'aah', 'aai', 'a1m', 'a3d', 'aaj', 'aak', 'a1n', 'a3e', 'aal', 'aam',
           'a1p', 'a3f', 'aan', 'aao', 'a1r', 'a3g', 'aap', 'aaq', 'a1s', 'a3h', 'aar', 'aas', 'a1t', 'a3j',
           'aat', 'aau', 'a1u', 'a3k', 'aav', 'aaw', 'a1v', 'a3l', 'aax', 'aay', 'a1w', 'a3m', 'da1', 'da5',
           'da2', 'da6', 'da3', 'da7', 'da4', 'da8', 'abb', 'abc', 'a1y', 'a3p', 'abd', 'abe', 'a1z', 'a3r',
           'abf', 'abg', 'a2a', 'a3s', 'abh', 'abi', 'a2b', 'a3t', 'abj', 'abk', 'a2c', 'a3u', 'abl', 'abm',
           'a2d', 'a3v', 'abn', 'abo', 'a2e', 'a3w', 'abp', 'abq', 'a2f', 'a3x', 'abr', 'abs', 'a2g', 'a3y',
           'abt', 'abu', 'a2h', 'a3z', 'abv', 'abw', 'a2j', 'a4a', 'abx', 'aby', 'a2k', 'a4b', 'abz', 'aca',
           'a2l', 'a4c', 'bb1', 'acb', 'acc', 'a2m', 'a4d', 'acd', 'ace', 'a2n', 'a4e', 'acf', 'acg', 'a2p',
           'a4f', 'ach', 'aci', 'a2r', 'a4g', 'acj', 'ack', 'a2t', 'a4h', 'acl', 'a2u', 'a4k', 'acm', 'a2v',
           'a4l', 'acn', 'a2w', 'a4m', 'aco', 'a1a', 'a2x', 'a4n', 'acp', 'acr', 'acs', 'act', 'a1q', 'a2q',
           'acu', 'acv', 'a3q', 'a4q', 'bda', 'b1k', 'bdb', 'b1l', 'bdc', 'b1m', 'bdd', 'b1n', 'bde', 'b1p',
           'bdf', 'b1r', 'bdg', 'b1s', 'bdh', 'b1t', 'bdi', 'b1u', 'bdj', 'b1v', 'bdk', 'b1w', 'bdl', 'b1x',
           'bdm', 'b1y', 'bdn', 'b1z', 'bdo', 'b2a', 'bdp', 'b2b', 'bdq', 'b2c', 'bdr', 'b2d', 'bds', 'b2e',
           'bdt', 'b2f', 'bdu', 'b2g', 'bdv', 'b2h', 'bdw', 'b2j', 'bdx', 'b2k', 'bdy', 'b2l', 'bdz', 'b2m',
           'bea', 'b2n', 'beb', 'b2p', 'bec', 'b2r', 'bed', 'b2s', 'bee', 'b2t', 'bef', 'b2y', 'beg', 'b3a',
           'beh', 'b3b', 'bei', 'bej', 'bek', 'cfa', 'cfb', 'cfc', 'c1c', 'cfd', 'cfe', 'cff', 'c1d', 'cfg',
           'cfh', 'cfi', 'c1e', 'cfj', 'cfk', 'cfl', 'c1f', 'cfm', 'c1g', 'cfn', 'c1h', 'cfo', 'c1j', 'cfp',
           'c1k', 'cfq', 'c1l', 'cfr', 'c1m', 'cfs', 'c1n', 'cft', 'c1p', 'cfu', 'c1r', 'cfv', 'c1s', 'cfw',
           'c1t', 'cfx', 'c1u', 'cfy', 'c1v', 'cfz', 'c1w', 'cga', 'c1x', 'cgb', 'c1y', 'cgc', 'c1z', 'cgd',
           'c2a', 'cge', 'c2b', 'cgf', 'c2c', 'cgg', 'c2d', 'cgh', 'c2e', 'cgi', 'c2f', 'cgj', 'c2g', 'cgk',
           'c2h', 'cgl', 'c2j', 'cgm', 'c2k', 'cgn', 'c2l', 'cgo', 'c2m', 'cgp', 'c2n', 'cgq', 'c2p', 'cgr',
           'c2r', 'cgs', 'c2s', 'cgt', 'c2t', 'cgu', 'c2u', 'cgv', 'c2v', 'cgw', 'c2w', 'dha', 'd1m', 'dhb',
           'd1n', 'dhc', 'd1p', 'dhd', 'd1r', 'dhe', 'd1s', 'dhf', 'd1t', 'dhg', 'd1u', 'dhh', 'd1v', 'dhi',
           'd1w', 'dhj', 'd1x', 'dhk', 'd1y', 'dhl', 'd1z', 'dhm', 'd2a', 'dhn', 'd2b', 'dhp', 'd2f', 'dhq',
           'd2h', 'eka', 'ekb', 'ekc', 'ekd', 'eke', 'ekf', 'eln', 'elo', 'elp', 'elq', 'elr', 'els', 'emy',
           'emz', 'ena', 'enb', 'enc', 'end', 'ene', 'enf', 'gpa', 'gpb', 'gpc', 'gpd', 'gpe', 'gpf', 'gpg',
           'gph', 'gqu', 'gqv', 'gqw', 'gqx', 'mss', 'm1w', 'mst', 'm1y', 'jsz', 'j2t', 'j3l', 'j4f', 'hue',
           'huf', 'hug', 'huh', 'huj', 'huk', 'jul', 'jum', 'jun', 'jup', 'jur', 'jus', 'juq', 'jut', 'juv',
           'juw', 'juu', 'j1c', 'j1r', 'jux', 'j1b', 'j1p', 'juy', 'j1d', 'j1s', 'juz', 'j1e', 'j1t', 'jva',
           'j1a', 'j1n', 'jvb', 'j1g', 'j1v', 'jvc', 'j1k', 'j1y', 'jvg', 'j2d', 'j2x', 'j3s', 'jvh', 'j2e',
           'j2y', 'j3t', 'jvi', 'j2r', 'j3j', 'j4d', 'jvj', 'jvl', 'jvk', 'jvm', 'j1f', 'j1u', 'jvn', 'j1h',
           'j1w', 'jvp', 'j1j', 'j1x', 'jvq', 'j2n', 'j3g', 'j4b', 'jvr', 'j1l', 'j1z', 'jvs', 'j1m', 'j2a',
           'jvt', 'jvu', 'jvv', 'jvw', 'j2l', 'j3e', 'j3z', 'jvx', 'j2m', 'j3f', 'j4a', 'jvy', 'jvz', 'j3n',
           'j4h', 'mxq', 'm1u', 'myp', 'm2j', 'myq', 'm1m', 'myr', 'm1n', 'mys', 'm1p', 'myt', 'm1t', 'myu',
           'm1v', 'myv', 'm1x', 'myw', 'm2a', 'myx', 'm2c', 'myy', 'm2d', 'myz', 'm2e', 'mze', 'mzj', 'm1z',
           'mzr', 'b1a', 'g1a', 'g1c', 'gsm', 'gsn', 'b1b', 'g1b', 'e1b', 'elt', 'b2u', 'g1d', 'gsp', 'elu',
           'e1w', 'gsr', 'a1c', 'e1c', 'b1c', 'b2v', 'elv', 'elw', 'e1x', 'e3n', 'eng', 'enh', 'eni', 'e3s',
           'e2e', 'e2f', 'e2g', 'e3t', 'h3t', 'enj', 'hta', 'htb', 'enk', 'e2h', 'htc', 'e3u', 'h3u', 'htd',
           'e0k', 'e0l', 'hte', 'htf', 'e0m', 'h3x', 'htm', 'htn', 'hto', 'h3z', 'hts', 'htt', 'htu', 'h4a',
           'e4a', 'h4b', 'e0u', 'e0v', 'h4c', 'e0w', 'h4d', 'h4e', 'h4f', 'h4g', 'h4h', 'h4j', 'h4k', 'h4l',
           'h4m', 'h4n', 'h4p', 'h4r', 'h4s', 'h4t', 'h4u', 'h4v', 'h4w', 'h4x', 'h4y', 'h4z', 'h5a', 'h5b',
           'h5c', 'h5d', 'h5e', 'h5f', 'h5h', 'h5j', 'h5k', 'h5l', 'h5m', 'h5n', 'h5p', 'h5r', 'h5s', 'h5t',
           'h5u', 'h5v', 'h5w', 'h5x', 'h5y', 'h5z', 'htv', 'htw', 'htx', 'h6a', 'h6b', 'h6c', 'h6d', 'h6e',
           'h6f', 'h6g', 'h6h', 'h6n', 'h6p', 'h6r', 'h6s', 'h6t', 'h6u', 'h6v', 'h6w', 'h6x', 'h6y', 'h6z',
           'h7a', 'd7b', 'h7b', 'hty', 'd7c', 'htz', 'hua', 'h7c', 'hub', 'huc', 'hud', 'j7y', 'j2b', 'j2v',
           'j3p', 'j7z', 'j2c', 'j2w', 'j3r', 'j8a', 'j2f', 'j2z', 'j3u', 'j8b', 'j2g', 'j3a', 'j3v', 'j8c',
           'j2h', 'j3b', 'j3w', 'j8d', 'j2j', 'j3c', 'j3x', 'j8e', 'j2k', 'j3d', 'j3y', 'j8j', 'j2p', 'j3h',
           'j4c', 'j8k', 'j2s', 'j3k', 'j4e', 'j8l', 'j2u', 'j3m', 'j4g', 'h9u', 'h9v', 'h9w', 'h9x', 'm9z',
           'm2b', 'etat_pub']


schema =  [{"name":str(c), "type":"string"} for c in columns]
db_output = 'publiques_complets'
db_input = 'comptes-annuels'
apikey = '65b42899cf863dc85b14b8c7989c09a5a3379607f22b6502bc0123e7'
url_base = 'https://opendata.datainfogreffe.fr/api/records/1.0/download/?'
db_format = 'json'
rows=''

excluded = {'millesime':[str(2000+i) for i in range(1,15)]}
exclusions = '&'.join(['exclude.'+k+'='+v for k,_ in excluded.items() for v in excluded[k]])

refined = {'date_de_publication':['2018/01']}
refinements = '&'.join(['refine.'+k+'='+v for k,_ in refined.items() for v in refined[k]])

other = []
other.append(exclusions)
other.append(refinements)

def url_construct(url_base=url_base,
        db_input=db_input,
        db_format=db_format,
        rows=rows,
        other=other,
        apikey=apikey
       ):
    url = ''
    url+=url_base+'dataset='+db_input+'&rows='+rows+'&format='+db_format
    for o in other:
        url+='&'+o
    url+='&apikey='+apikey+';'
    return url





def get_url(url):
    r = requests.get(url)
    data=json.dumps(r.json())
    parsed_data=json.loads(data)
    df=pd.DataFrame(parsed_data)
    return df

def get_url(url):
    r = requests.get(url)
    data=json.dumps(r.json())
    parsed_data=json.loads(data)
    df=pd.DataFrame(parsed_data)
    return df

# Extract historical dates
end_date = date.today()-timedelta(date.today().day+1)
dates = ["2016-01-01", end_date.strftime("%Y-%m-%d")]
start, end = [datetime.strptime(_, "%Y-%m-%d") for _ in dates]
list_dates = OrderedDict(((start + timedelta(_)).strftime(r"%Y/%m"), None) for _ in xrange((end - start).days)).keys()

# Schema outputs preparation : replace dss function create instance 'publiques_complets' form class Dataset and write shema
#publiques_complets = dataiku.Dataset(db_output)
#publiques_complets.write_schema(schema)

#replace dss function :  Writing in a Dataset 'publiques_complets' by chunks of dataframes

#with publiques_complets.get_writer() as writer:
    for year_month in list_dates:
        # year,month = year_month.split('-')
        print year_month

        df = pd.DataFrame()
        other = []
        other.append(exclusions)
        refined = {'date_de_publication':[year_month]}
        refinements = '&'.join(['refine.'+k+'='+v for k,_ in refined.items() for v in refined[k]])
        other.append(refinements)
        url = url_construct(other=other)
        try:
            df = get_url(url)
        except : print "Exception in "+year_month

        if df.empty:
            continue
            print "Empty set in "+ year_month
        else:
            normalized_data=json_normalize(df['fields'])
            data_listed=normalized_data['fields'].tolist()
            temp_data = pd.DataFrame(data=data_listed)
            final_df=pd.DataFrame(data=temp_data, columns=columns)
            writer.write_dataframe(final_df)