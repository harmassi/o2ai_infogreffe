import pandas as pd, numpy as np
from datetime import datetime
data = pd.read_csv('data/pcl_prepared.csv')
data['date_ouverture_pcl'] = pd.to_datetime(data['date_ouverture_pcl'])
data = data[['date_ouverture_pcl'<'09-02-2050']]
data = data.sort_values(by=['date_publication'],ascending=False).groupby('siren').agg({'date_pubication':"first",
                                                                                          'date_ouverture_pcl':'max'})
pcl_prepared_max_date = data[['siren','date_publication','date_ouverture_pcl']]

for col in pcl_prepared_max_date.columns:
    print(pcl_prepared_max_date[col])