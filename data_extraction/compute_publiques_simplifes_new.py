import pandas as pd, numpy as np
import json
import requests
from pandas.io.json import json_normalize
import csv
import itertools
from datetime import datetime, timedelta, date
from collections import OrderedDict

# Column preparation
columns = ['denomination', 'siren', 'nic', 'forme_juridique', 'code_ape', 'libelle_ape', 'adresse',
           'code_postal', 'ville', 'num_dept', 'departement', 'region', 'code_greffe', 'greffe',
           'date_immatriculation', 'date_radiation', 'statut', 'geolocalisation',
           'date_de_publication', 'type', 'millesime', 'date_depot', 'date_cloture',
           'date_cloture_n_1', 'date_saisie', 'confidentiel', 'type_anomalie', 'devise', 'duree_exercice',
           'duree_exercice_n_1', '10', '12', '13', 'n00', '14', '16', '17', 'n01', '28', '30', '31', 'n02',
           '40', '42', '43', 'n03', '44', '48', '49', 'n04', '50', '52', '53', 'n05', '60', '62', '63',
           'n06', '64', '66', '67', 'n07', '68', '70', '71', 'n08', '72', '74', '75', 'n09', '80', '82',
           '83', 'n10', '84', '86', '87', 'n11', '88', '90', '91', 'n12', '92', '94', '95', 'n13', '96',
           '98', '99', 'n14', '110', '112', '113', 'n15', '120', 'n16', '124', 'n17', '126', 'n18', '130',
           'n19', '132', 'n20', '134', 'n21', '136', 'n22', '140', 'n23', '142', 'n24', '154', 'n25',
           '156', 'n26', '164', 'n27', '166', 'n28', '169', '172', 'n29', '174', 'n30', '176', 'n31',
           '180', 'n32', '182', '184', '193', '195', '197', '199', '209', '210', 'n33', '214', 'n34',
           '215', '217', '218', 'n35', '222', 'n36', '224', 'n37', '226', 'n38', '230', 'n39', '232',
           'n40', '234', 'n41', '236', 'n42', '238', 'n43', '24a', '24b', '240', 'n44', '242', 'n45',
           '243', '244', 'n46', '250', 'n47', '252', 'n48', '254', 'n49', '256', 'n50', '259', '262',
           'n51', '264', 'n52', '270', 'n53', '280', 'n54', '290', 'n55', '294', 'n56', '300', 'n57',
           '306', 'n58', '310', 'n59', '316', '374', '376', '378', '402', '404', '412', '414', '422',
           '432', '442', '452', '462', '472', '482', '484', '490', '492', '494', '582', '584', '596',
           '598', '602', '603', '604', '605', '612', '614', '622', '624', '632', '634', '642', '644',
           '652', '654', '662', '664', '682', '684', 'etat_pub']

schema = [{"name": str(c), "type": "string"} for c in columns]
db_output = 'publiques_simplifies_new'
db_input = 'comptes-annuels-simplifies'
apikey = '65b42899cf863dc85b14b8c7989c09a5a3379607f22b6502bc0123e7'
url_base = 'https://opendata.datainfogreffe.fr/api/records/1.0/download/?'
db_format = 'json'
rows = ''

excluded = {'millesime': [str(2000 + i) for i in range(1, 15)]}
exclusions = '&'.join(['exclude.' + k + '=' + v for k, _ in excluded.items() for v in excluded[k]])

refined = {'date_de_publication': ['2018/01']}
refinements = '&'.join(['refine.' + k + '=' + v for k, _ in refined.items() for v in refined[k]])

other = []
other.append(exclusions)
other.append(refinements)


def url_construct(url_base=url_base,
                  db_input=db_input,
                  db_format=db_format,
                  rows=rows,
                  other=other,
                  apikey=apikey
                  ):
    url = ''
    url += url_base + 'dataset=' + db_input + '&rows=' + rows + '&format=' + db_format
    for o in other:
        url += '&' + o
    url += '&apikey=' + apikey + ';'
    return url


def get_url(url):
    r = requests.get(url)
    data = json.dumps(r.json())
    parsed_data = json.loads(data)
    df = pd.DataFrame(parsed_data)
    return df


# Extract dates
today_date = date.today()
current_month = today_date.strftime("%Y/%m")
list_dates = [current_month]

#replace dss functions : create instance 'publiques_simplifies_new' form class Dataset and write shema

#confidentiels_complets = dataiku.Dataset(db_output)
#confidentiels_complets.write_schema(schema)

#replace dss function :  Writing in a Dataset 'publiques_simplifies_new' by chunks of dataframes

#with confidentiels_complets.get_writer() as writer:
    for year_month in list_dates:
        # year,month = year_month.split('-')
        print
        year_month

        df = pd.DataFrame()
        other = []
        other.append(exclusions)
        refined = {'date_de_publication': [year_month]}
        refinements = '&'.join(['refine.' + k + '=' + v for k, _ in refined.items() for v in refined[k]])
        other.append(refinements)
        url = url_construct(other=other)
        try:
            df = get_url(url)
        except:
            print
        "Exception in " + year_month

        if df.empty:
            continue
            print
            "Empty set in " + year_month
        else:
            normalized_data = json_normalize(df['fields'])
            data_listed = normalized_data['fields'].tolist()
            temp_data = pd.DataFrame(data=data_listed)
            final_df = pd.DataFrame(data=temp_data, columns=columns)
            writer.write_dataframe(final_df)


