import pandas as pd, numpy as np
import csv
from datetime import datetime

data = pd.read_csv('data/calcul_note_explications.csv')
data = data.head()
data =  data.drop(['proba_percentile_normalized','prediction','proba_percentile','procedure_collective'],axis=1)
data['code_greffe'] = data['code_greffe'].apply(lambda x :round(x))
data['dttimestamp'] = datetime.now()
calcul_note_prepared = data