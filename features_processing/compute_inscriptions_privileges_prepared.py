import pandas as pd
from datetime import datetime
def is_not_blank(s):
    return bool(pd.notna(s) and str(s).strip())
def process(row):

    correct_date_inscription = row['date_radiation_inscription'] if is_not_blank(row['date_radiation_inscription']) else row['date_inscription']
    return correct_date_inscription

data = pd.read_csv('data/inscriptions_privileges_stacked.csv')

data['correct_evenement'] = data.apply(lambda row :  process(row) ,axis=1)
data= data.dropna(subset=['siren'])

data['correct_evenement'] = data['correct_evenement'].apply(lambda x : datetime.strptime(str(x), '%Y-%m-%d'))

data = data.drop(['date_radiation_inscription', 'date_inscription'], axis=1)
data['montant_creance'] = data['montant_creance'].apply(lambda x : round(x))
inscriptions_privileges_prepared = data