import sqlite3
baseDeDonnees = sqlite3.connect('contacts.db')
curseur = baseDeDonnees.cursor()
curseur.execute("""
SELECT
    "INFOGREFFE_inscriptions_privileges_prepared"."num_inscription" AS "num_inscription",
    "INFOGREFFE_inscriptions_privileges_prepared"."date_evenement" AS "date_evenement",
    "INFOGREFFE_inscriptions_privileges_prepared"."montant_creance" AS "montant_creance",
    "INFOGREFFE_inscriptions_privileges_prepared"."siren" AS "siren",
    "INFOGREFFE_inscriptions_privileges_prepared"."denomination" AS "denomination",
    "INFOGREFFE_inscriptions_privileges_prepared"."etat" AS "etat"
FROM
    "INFOGREFFE_inscriptions_privileges_prepared"
    INNER JOIN (
        SELECT
            "siren" AS "siren",
            "num_inscription" AS "num_inscription",
            MAX("date_evenement") AS "date_evenement_max",
            COUNT(*) AS "count"
        FROM
            (
                SELECT
                    "num_inscription" AS "num_inscription",
                    "date_evenement" AS "date_evenement",
                    "montant_creance" AS "montant_creance",
                    "siren" AS "siren",
                    "denomination" AS "denomination",
                    "etat" AS "etat"
                FROM
                    "INFOGREFFE_inscriptions_privileges_prepared"
            ) "dku__beforegrouping"
        GROUP BY
            "siren",
            "num_inscription"
    ) AS t_inner ON "INFOGREFFE_inscriptions_privileges_prepared"."siren" = t_inner."siren"
    AND "INFOGREFFE_inscriptions_privileges_prepared"."num_inscription" = t_inner."num_inscription"
    AND "INFOGREFFE_inscriptions_privileges_prepared"."date_evenement" = t_inner."date_evenement_max"
""")
baseDeDonnees.commit()
baseDeDonnees.close()
