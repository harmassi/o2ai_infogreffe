import pandas as pd
from datetime import datetime

data = pd.read_csv('data/pcl_stacked.csv')
data = data.dropna(subset=['siren','date_ouverture_pcl'])
data['siren'] = data['siren'].apply(lambda x : round(x))
data['date_ouverture_pcl'] =  data['date_ouverture_pcl'].apply(lambda x : datetime.strptime(str(x), '%Y-%m-%d').isoformat() + ".000Z")
pcl_prepared = data

