from datetime import datetime
import pandas as pd

data = pd.read_csv('data/stacked_all.csv')

data['date_immatriculation'] =  data['date_immatriculation'].apply(lambda x : datetime.strptime(str(x), '%Y-%m-%d'))
data['date_de_publication'] =  data['date_de_publication'].apply(lambda x : datetime.strptime(str(x), '%Y-%m-%d'))
data['date_depot'] =  data['date_depot'].apply(lambda x : datetime.strptime(str(x), '%Y-%m-%d'))
data['date_cloture'] =  data['date_cloture'].apply(lambda x : datetime.strptime(str(x), '%Y-%m-%d'))
stacked_schema = data